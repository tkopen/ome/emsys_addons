# -*- coding: utf-8 -*-
from openerp import http

class Emsys(http.Controller):
    @http.route('/emsys/emsys/', auth='public')
    def index(self, **kw):
        return "Hello, world"

#     @http.route('/emsys/emsys/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('emsys.listing', {
#             'root': '/emsys/emsys',
#             'objects': http.request.env['emsys.emsys'].search([]),
#         })

#     @http.route('/emsys/emsys/objects/<model("emsys.emsys"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('emsys.object', {
#             'object': obj
#         })