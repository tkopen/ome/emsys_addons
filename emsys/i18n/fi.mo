��    >        S   �      H     I     P     b     k     |  	   �     �     �     �     �     �     �     �     �     �                    &     >     O  	   X  	   b     l     t     �     �     �     �     �     �  +   �           )  	   8     B     H     P     i     w     }     �     �     �  	   �     �     �     �     �     �     �     
  	        &     7     G  �   M     �     �  
   	     	  �  .	     �
     �
  
   �
       	             #     2     ?     M     T     \     b     o     �     �     �     �     �      �                    ,     2     D     T     q     �     �     �  1   �  	   �     �     �     �       7     
   C     N     T     Z     c  
   s     ~  .   �     �     �     �     �     �               3     E     Y    _  	   h     r  	   �     �     >   :   $                0   3   *   +      ;                !       6                /         ,   8                =   9   '   .   1         #           )                 4      &              7                 <   "       
                  (              2                 	   -       5   %       Amount Cancelled Invoice Comment: Company Registry Company Registry: Customer: Date Ordered: Delivery Date Delivery Date: Description Description: Disc.% Draft Invoice Due Date Emsys Invoice Euro Expiry Date Expiry Date: Fiscal Position Remark: From account no. Incoterm Incoterm: Incoterms Invoice Invoice Date: Invoice address: Invoice and shipping address: Invoice details: Invoice number Invoice number: Invoice reference: Invoice summary (details on the next page): Order No Our reference: PRO-FORMA Page: Partner Payer's name and address Payment Term: Price Product Quantity Quotation Date: Quotation No Recipient Recipient's account number Ref. No. Reference Number Refund Sales Order Salesperson: Shipping address: Signature Supplier Invoice Supplier Refund Taxes The payment will be cleared for the recipient in accordance with the General terms for payment transmission and only on the basis of the account number given by the payer. Total Total Without Taxes Unit Price Your Reference: Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-23 08:02+0000
PO-Revision-Date: 2016-05-23 11:04+0200
Last-Translator: <>
Language-Team: Finnish <http://www.transifex.com/odoo/odoo-8/language/fi/>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.10
X-Poedit-SourceCharset: UTF-8
 Summa Peruutettu lasku Kommentti: Y-tunnus Y-tunnus: Asiakas: Tilauspäivä: Toimitusaika Toimitusaika: Kuvaus Kuvaus: Ale % Laskuehdotus Eräpäivä<br />Förfallodag Emsys Lasku Euro Voimassaolopäivä Voimassaolopäivä: Tilikausiposition huomautus: Tililtä nro<br />Från konto nr Toimitusehto Toimitusehto: Toimitusehdot Lasku Laskun päiväys: Laskutusosoite: Laskutus- ja toimitusosoite: Laskun erittely: Laskun numero Laskun numero: Viitenumero: Laskun yhteenveto (erittely seuraavalla sivulla): Tilausnro Viitteemme: Proforma Sivu: Asiakas Maksajan nimi ja osoite<br />Betalarens namn och adress Maksuehto: Hinta Tuote Määrä Tarjouspäivä: Tarjousnro Saaja<br />Mottagare Saajan tilinumero<br />Mottagarens kontonummer Viitenumero<br />Ref. nr Viitenumero Hyvitys Myyntitilaus Myyjä: Toimitusosoite: Allekirjoitus<br />Underskrift Toimittajan lasku Toimittajan hyvitys Verot Maksu välitetään saajalle maksujenvälityksen ehtojen mukaisesti ja vain maksajan ilmoittaman tilinumeron perusteella.<br />Betalningen förmedlas till mottagaren enligt villkoren för betalningsförmedling och endast till det kontonummer som betalaren angivit. Yhteensä Veroton kokonaishinta Yks.hinta Viite/kuvaus: 