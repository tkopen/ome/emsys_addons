# -*- coding: utf-8 -*-
{
    'name': "Emsys Invoice",

    'summary': """
        Custom Finnish invoice and quotation template""",
    'description': """
		This module adds a Finnish style quotation and invoice template to the Sales View""",
    'author': "Tapio Pensasmaa",
    'license': "AGPL-3",
    'website': "http://www.emsystems.fi",
    'category': 'EMsys',
    'version': '8.0.0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'sale',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/report_emsys.xml',
        'views/layout_emsys.xml',
        'views/saleorder_emsys.xml',
        'data/emsys_incoterms_code.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
    'installable': True,
    'application': True,
}
