# -*- coding: utf-8 -*-
from openerp import api, fields, models
from openerp.tools.translate import _
import re
import logging

log = logging.getLogger(__name__)

# Y-tunnus asiakaslomakkeeseen
class EmsysPartner(models.Model):
    _inherit = 'res.partner'
    x_businessid = fields.Char('Company Registry')


class EmsysOrder(models.Model):
    _inherit = 'sale.order'

    x_validity_date = fields.Date('Expiry Date')
    x_incoterm = fields.Many2one('incoterms_code', 'Incoterm')
    # x_incoterm = fields.Char('Incoterm')
    x_delivery_date = fields.Date('Delivery Date')

    def print_quotation(self, cr, uid, ids, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time'
        self.signal_workflow(cr, uid, ids, 'quotation_sent')
        return self.pool['report'].get_action(cr, uid, ids, 'emsys.saleorder_emsys', context=context)


class EmsysIncoterm(models.Model):
    _name = "incoterms_code"
    name = fields.Char('Incoterms')


class EmsysReport(models.Model):
    _inherit = 'account.invoice'

    @api.one
    @api.depends('number', 'state')
    def _compute_ref_number(self):
        if self.number:
            invoice_number = re.sub(r'\D', '', self.number)
            checksum = sum((7, 3, 1)[idx % 3] * int(val)
                           for idx, val in enumerate(invoice_number[::-1]))
            self.ref_number = invoice_number + str((10 - (checksum % 10)) % 10)
            self.invoice_number = invoice_number
        else:
            self.invoice_number = False
            self.ref_number = False

    @api.one
    def _compute_barcode_string(self):
        primary_bank_account = self.partner_bank_id or \
                               self.company_id.bank_ids and self.company_id.bank_ids[0]
        if (self.amount_total and primary_bank_account.acc_number
            and self.ref_number and self.date_due):
            amount_total_string = str(self.amount_total)
            if amount_total_string[-2:-1] == '.':
                amount_total_string += '0'
            amount_total_string = amount_total_string.zfill(9)
            receiver_bank_account = re \
                .sub("[^0-9]", "", str(primary_bank_account.acc_number))
            ref_number_filled = self.ref_number.zfill(20)
            self.barcode_string = '4' \
                                  + receiver_bank_account \
                                  + amount_total_string[:-3] \
                                  + amount_total_string[-2:] \
                                  + "000" + ref_number_filled \
                                  + str(self.date_due)[2:4] \
                                  + str(self.date_due)[5:-3] \
                                  + str(self.date_due)[-2:]
        else:
            self.barcode_string = False

    invoice_number = fields.Char(
        'Invoice number',
        compute='_compute_ref_number',
        store=True,
        help=_('Identifier number used to refer to this invoice in '
               'accordance with https://www.fkl.fi/teemasivut/sepa/'
               'tekninen_dokumentaatio/Dokumentit/kotimaisen_viitte'
               'en_rakenneohje.pdf')
    )

    ref_number = fields.Char(
        'Reference Number',
        compute='_compute_ref_number',
        store=True,
        help=_('Invoice reference number in accordance with https://'
               'www.fkl.fi/teemasivut/sepa/tekninen_dokumentaatio/Do'
               'kumentit/kotimaisen_viitteen_rakenneohje.pdf')
    )

    # date_delivered = fields.Date(
    #     'Date delivered',
    #     help=_('The date when the invoiced product or service was considered '
    #            'delivered, for taxation purposes.')
    # )

    barcode_string = fields.Char(
        'Barcode String',
        compute='_compute_barcode_string',
        help=_('https://www.fkl.fi/teemasivut/sepa/tekninen_dokumentaatio/Dok'
               'umentit/Pankkiviivakoodi-opas.pdf')
    )

    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'emsys.report_emsys')
