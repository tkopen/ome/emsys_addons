# -*- coding: utf-8 -*-

from openerp import models, fields, api


# Notice the _name attribute absent. It is not needed because it is already inherited from the parent model.
class EmsysContacts(models.Model):
    _inherit = 'res.partner'
    tol_2008 = fields.Many2one('tol_main', 'TOL2008 Main Category')
    comp_category = fields.Char(string='Comp. Category/Products')
    desc_customers = fields.Char(string='Customers')
    principals = fields.Char(string='Principals')
    core_competency = fields.Char(string='Core Competency')
    competitors = fields.Char(string='Competitors')
    inoa_url = fields.Char(string='Inoa url')
    contacted = fields.Boolean(string='Contacted')
    turnover = fields.Many2one('year_turnover', 'Turnover')
    trend_turnover = fields.Many2one('trend_turno', 'Turnover Trend')
    trend_profit = fields.Many2one('trend_prof', 'Profit Trend')
    number_staff = fields.Many2one('staffno', 'Number of Staff')
    comp_type = fields.Many2one('company_type', 'Company Type')
    lead_source = fields.Many2one('lead_src', 'Lead Source')
    responsibility = fields.Many2one('duty', 'Responsibility')
    education = fields.Char('Education')
    birthday = fields.Date('Date of Birth')
    contact_method = fields.Char('Method of Contact')
    linkedin = fields.Char('LinkedIn')
    twitter = fields.Char('Twitter')
    skype = fields.Char('Skype')


class Turnover(models.Model):
    _name = "year_turnover"
    name = fields.Char('Turnover')


class NumberStaff(models.Model):
    _name = "staffno"
    name = fields.Char('Number of Staff')


class CompanyType(models.Model):
    _name = "company_type"
    name = fields.Char('Company Type')


class LeadSource(models.Model):
    _name = "lead_src"
    name = fields.Char('Lead Source')


class Responsibility(models.Model):
    _name = "duty"
    name = fields.Char('Responsibility')


class TrendTurnover(models.Model):
    _name = "trend_turno"
    name = fields.Char('Turnover Trend')

class TrendProfit(models.Model):
    _name = "trend_prof"
    name = fields.Char('Profit Trend')

class Tol2008Main(models.Model):
    _name = "tol_main"
    name = fields.Char('TOL2008 Main Category')