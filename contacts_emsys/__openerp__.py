# -*- coding: utf-8 -*-
{
    'name': "Emsys Contacts",
    'summary': """
		This module adds a new Tab Lisatietoja to the Sales/Customer form""",
	'description': """
		This module adds a new tab view Lisatietoja with many fields to the Sales/Customer (Company) form.
		This data is imported from a previously used VTigerCRM to Odoo v8.0""",
    'author': "Tapio Pensasmaa",
    'license': "AGPL-3",
    'website': "http://www.emsystems.fi",
    'category': 'EMsys',
    'version': '8.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/contacts_emsys_views.xml',
        'data/contacts_emsys_data.xml',
        'data/res.partner.category.csv',
        'data/tol_main.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
    'installable':True,
    'application':True,
}
